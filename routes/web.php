<?php

use App\Http\Controllers\karyawanCont;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});

Route::get('/karyawan',[karyawanCont::class,'index']);
Route::get('/add',[karyawanCont::class,'add']);
Route::post('/store',[karyawanCont::class,'store']);
Route::get('/edit/{id}',[karyawanCont::class,'edit']);
Route::post('/update',[karyawanCont::class,'update']);
Route::get('/delete/{id}',[karyawanCont::class,'delete']);