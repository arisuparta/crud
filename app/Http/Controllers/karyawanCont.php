<?php

namespace App\Http\Controllers;

use App\Models\Karyawan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class karyawanCont extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_karyawan = DB::table('data_karyawan')->get();
        //memanggil database
        return view('karyawan', ['data_karyawan' => $data_karyawan]);
        //return view('karyawan');
    }
    //untuk menampilkan view tambah data 
    public function add()
    {
        //memanggil view tambah
        return view('create');
    }
    public function store(Request $request)
    {
        //insert data ke table Karyawan

        DB::table('data_karyawan')->insert(
            [
                'nama_karyawan' => $request->nama_karyawan,
                'no_karyawan' => $request->no_karyawan,
                'no_telp_karyawan' => $request->no_telp_karyawan,
                'jabatan_karyawan' => $request->jabatan_karyawan,
                'devisi_karyawan' => $request->devisi_karyawan
            ]
        );
        // alihkan halaman ke halaman karyawan
        return redirect('/karyawan')->with('sukses', 'Data berhasil diinput!');
    }
    // method untuk edit data karyawan
    public function edit($id)
    {
        // mengambil data karyawan berdasarkan id yang dipilih
        $data_karyawan = DB::table('data_karyawan')->where('id', $id)->get();

        // passing data karyawan yang didapat ke view edit.blade.php 
        return view('edit', ['data_karyawan' => $data_karyawan]);
    }
    // update data karyawan
    public function update(Request $request)
    {
        // update data karyawan
        DB::table('data_karyawan')->where('id', $request->id)->update([

            'nama_karyawan' => $request->nama_karyawan,
            'no_karyawan' => $request->no_karyawan,
            'no_telp_karyawan' => $request->no_telp_karyawan,
            'jabatan_karyawan' => $request->jabatan_karyawan,
            'devisi_karyawan' => $request->devisi_karyawan
        ]);

        // alihkan halaman ke halaman karyawan
        return redirect('/karyawan')->with('sukses', 'Data berhasil diupdate!');
    }

    // method untuk hapus data karyawan
    public function delete($id)
    {
        // menghapus data karyawan berdasarkan id yang dipilih
        DB::table('data_karyawan')->where('id', $id)->delete();

        // alihkan halaman ke halaman karyawan
        return redirect('/karyawan')->with('sukses', 'Data berhasil dihapus!');
    }
}
