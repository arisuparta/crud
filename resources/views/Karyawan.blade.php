@extends('layouts.main')

@section('container')

    @if (session('sukses'))
        <div class="alert alert-success mt-2" role="alert">
            {{ session('sukses') }}
        </div>
    @endif
    <div class="row">
        <div class="col-9">
            {{-- <h1>Sistem Informasi Akademik</h1> --}}
            <h1>Data Karyawan</h1>
        </div>
        <div class="col-3">
            <a href="/add" class="btn btn-primary mt-2 pull-right">Tambah Data </a>
        </div>
        <table class="table table-hover table-bordered border-dark table-striped">
            <thead class="table-dark">
                <tr>
                    <th>Nama</th>
                    <th>NO</th>
                    <th>NO TELP</th>
                    <th>JABATAN</th>
                    <th>DEVISI</th>
                    <th>EDIT</th>
                </tr>
            </thead>
            @foreach ($data_karyawan as $p)
                <tr>
                    <td>{{ $p->nama_karyawan }}</td>
                    <td>{{ $p->no_karyawan }}</td>
                    <td>{{ $p->no_telp_karyawan }}</td>
                    <td>{{ $p->jabatan_karyawan }}</td>
                    <td>{{ $p->devisi_karyawan }}</td>
                    <td>
                        <a href="/edit/{{ $p->id }}" class="btn btn-success"><i class="fa fa-pencil"></i> Edit</a>
                        |
                        <a href="/delete/{{ $p->id }}" class="btn btn-danger"
                            onclick="return confirm('Apakah yakin ingin dihapus?')"><i class="fa fa-trash"></i> Hapus</a>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>
@endsection
