@extends('layouts.main')

@section('container')
    <div class="row">
        <div class="col-9">
            <h1>Update Data Karyawan</h1>
        </div>
        <div class="col-3">
            <a href="/" class="btn btn-primary mt-2 pull-right">Kembali</a>
        </div>
        <br />

        @foreach ($data_karyawan as $s)
            <form action="/update" method="POST">
                {{ csrf_field() }}
                <input type="hidden" name="id" value="{{ $s->id }}">
                <br />
                <div class="mb-3">
                    <label class="form-label">Nama</label>
                    <input type="text" class="form-control" name="nama_karyawan" required="required"
                        value="{{ $s->nama_karyawan }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">No</label>
                    <input type="text" class="form-control" name="no_karyawan" required="required"
                        value="{{ $s->no_karyawan }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">No Telp</label>
                    <input type="text" class="form-control" name="no_telp_karyawan" required="required"
                        value="{{ $s->no_telp_karyawan }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Jabatan</label>
                    <input type="text" class="form-control" name="jabatan_karyawan" required="required"
                        value="{{ $s->jabatan_karyawan }}">
                </div>
                <div class="mb-3">
                    <label class="form-label">Devisi</label>
                    <input type="text" class="form-control" name="devisi_karyawan" required="required"
                        value="{{ $s->devisi_karyawan }}">
                </div>
                <button type="submit" class="btn btn-primary">Update Data</button>
            </form>
        @endforeach
        <br>
    </div>
@endsection
