@extends('layouts.main')

@section('container')
    <div class="row">
        <div class="col-9">
            <h1>Tambah Data Karyawan</h1>
        </div>
        <div class="col-3">
            <a href="/karyawan" class="btn btn-primary mt-2 pull-right">Kembali</a>
        </div>
        <br />

        <form action="/store" method="POST">
            {{ csrf_field() }}
            <div class="mb-3">
                <label class="form-label">Nama </label>
                <input type="text" class="form-control" name="nama_karyawan" required="required">
            </div>
            <div class="mb-3">
                <label class="form-label">No</label>
                <input type="text" class="form-control" name="no_karyawan" required="required">
            </div>
            <div class="mb-3">
                <label class="form-label">No Telp</label>
                <input type="text" class="form-control" name="no_telp_karyawan" required="required">
            </div>
            <div class="mb-3">
                <label class="form-label">Jabatan</label>
                <input type="text" class="form-control" name="jabatan_karyawan" required="required">
            </div>
            <div class="mb-3">
                <label class="form-label">Devisi</label>
                <input type="text" class="form-control" name="devisi_karyawan" required="required">
            </div>
            <button type="submit" class="btn btn-primary">Simpan Data</button>
        </form>
        <br>
    </div>
@endsection
